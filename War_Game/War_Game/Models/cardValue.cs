﻿using System;
using System.Collections.Generic;
using System.Text;

namespace War_Game.Models
{
    public enum CardValue
    {
        roi,
        dame,
        valet,
        dix,
        neuf,
        huit,
        sept,
        six,
        cinq,
        quatre,
        trois,
        deux,
        un,
    }
}
