﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using War_Game.Models;

namespace War_Game.Models
{
    class Card
    {
        public CardType type;
        public CardValue value;
        public Card(CardType t, CardValue v)
        {
            this.type = t;
            this.value = v;
        }

        public string compare(Card c)
        {

            if (this.value == c.value)
            {
                return "draw";
            }

            // different cards case
            var values = Enum.GetValues(typeof(CardValue)).Cast<CardValue>();
            for (int i = 0; i < values.Count(); i++)
            {
                if (this.value == values.ElementAt(i))
                {
                    return "winner";
                }

                if (this.value == values.ElementAt(i))
                {
                    return "loser";
                }
            }

            return "error in compare";
        }

        public override string ToString()
        {
            return value + " of " + type;
        }
    }
}
