﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using War_Game.Models;

namespace War_Game.Controllers
{
    class Deck
    {
        public List<Card> cards;
        private String name;
        
        public void add( Card c)
        {
            this.cards.Add(c);
        }

        public string compare(Card c1, Card c2)
        {

            if (c1.value == c2.value)
            {
                return "draw";
            }

            // different cards case


            var values = Enum.GetValues(typeof(CardValue)).Cast<CardValue>();
            for (int i = 0; i < values.Count(); i++)
            {
                if (c1.value == values.ElementAt(i))
                {
                    return $"{c1.ToString()} wins over {c2.ToString()}";
                }

                if (c2.value == values.ElementAt(i))
                {
                    return $"{c2.ToString()} wins over {c1.ToString()}";
                }
            }
         
            return "error in compare";
        }

        public int getDeckSize()
        {
            return this.cards.Count;
        }

        //rand.Next(101)
        public Card pickCard()
        {
            Random rand = new Random();
            // random between 0 and cards size
            int index = rand.Next(cards.Count);
            Card ret = this.cards[index];
            this.cards.RemoveAt(index);
            return ret;
        }

        public void removeCard(Card card)
        {
            for (int i = 0; i < cards.Count; i++)
            {
                if (this.compare(card, cards[i]) == "draw") // todo compare
                {
                    this.cards.RemoveAt(i);
                    return;
                }
            }
        }

        public void shuffle()
        {
            Random rand = new Random();
            int n = cards.Count;
            while (n > 1)
            {
                n--;
                int i = rand.Next(n + 1);
                Card value = cards[i];
                cards[i] = cards[n];
                cards[n] = value;
            }

        }

    }
}
