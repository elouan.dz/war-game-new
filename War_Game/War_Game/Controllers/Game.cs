﻿using System;
using System.Collections.Generic;
using System.Text;
using War_Game.Models;

namespace War_Game.Controllers
{
    class Game
    {
        public Deck battleDeck;
        public Player playerOne;
        public Player playerTwo;

        public Boolean checkVictory (Player p)
        {
            if (p.playerDeck.getDeckSize() + p.victoryDeck.getDeckSize() == 52) // p has all cards of the game
            {
                return true;
            }
            return false;
        }

        public void battleRound ()
        {
            // both players plays a card
            Card p1Card = playerOne.playerDeck.pickCard();
            Card p2Card = playerTwo.playerDeck.pickCard();
            battleDeck.add(p1Card);
            battleDeck.add(p2Card);

            // comparaison of the cards
            if (p1Card.compare(p2Card) == "draw")
            {
                // todo bataille
            }
            else if (p1Card.compare(p2Card) == "win")
            {
                foreach (Card c in this.battleDeck.cards)
                {
                    playerOne.victoryDeck.add(c);
                }
            }
            else if (p1Card.compare(p2Card) == "lose")
            {
                foreach (Card c in this.battleDeck.cards)
                {
                    playerTwo.victoryDeck.add(c);
                }
            }
            else
            {
                Console.WriteLine("error when comparing the cards ! :'( ");
            }

            // check if a player is victorious
            if (this.checkVictory(playerOne))
            {
                // todo player1 is victorious
            }

            if (this.checkVictory(playerTwo))
            {
                // todo player2 is victorious
            }

        }
    }
}
