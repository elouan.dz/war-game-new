﻿using System;
using System.Collections.Generic;
using System.Text;
using War_Game.Models;

namespace War_Game.Controllers
{
    class Player
    {
        public Deck playerDeck { get; set; }
        public Deck victoryDeck { get; set; }

        public Boolean playerDeckEmpty ()
        {
            return playerDeck.getDeckSize() == 0 ? true : false;
        }

        public Boolean victoryDeckEmpty()
        {
            return victoryDeck.getDeckSize() == 0 ? true : false;
        }

        public Card play()
        {
            return playerDeck.pickCard();
        }
    }
}
